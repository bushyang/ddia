# Chp6 - Partitioning

## Introduction

- Each partition is a small database of its own
- Different partition can be placed on different node for scalability

## Partitioning and Replication

- Partition can be replicated to different node for fault tolerance
- Each node acts as leader for some partitions and follower for other partitions.

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled.png)

Figure 6-1. Combining replication and partitioning

## Partitioning of Key-Value Data

- Hot spot issue (all load come to that point)
- Assign records to nodes randomly
    - When trying to read an item, it has to query all nodes 👎

### Partitioning by Key Range

- The partition boundaries need to adapt to the data
    - Manually by administrator
    - Automatically
- Range scans are efficient 👍
    - Keys may be sorted in each partition
- Certain access pattern lead to hot spots 👎
    - EX: partitioned by timestamp
    - Solution: Prefixing timestamp with other information, hash key partitioning

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%201.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%201.png)

Figure 6-2. A print encyclopedia is partitioned by key range.

### Partitioning by Hash of Key

- The hash function need not to be cryptographically strong
    - EX:  Cassandra and MongoDB use MD5, and Voldemort uses the Fowler–
    Noll–Vo function
- Built-in hash function may not be suitable for partitioning (the same key may have different hash value in different processes)
- Good at distributing keys fairly among the partitions (known as consistent hashing) 👍
- Bad at range queries 👎
- Concatenated index approach by Cassandra (combine two strategy)
    - Compound primary key consisting of several columns
    - Only the first part of the column is hashed to determine partition
    - Can perform efficient range query over the other columns of key
    - Good for one-to-many relation👍

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%202.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%202.png)

Figure 6-3. Partitioning by hash of key.

### Skewed Workloads and Relieving Hot Spots

- Certain workload can still result requesting to same partition. Ex: Celebrity user
- Today, most data system relies on the application to reduce the skew
- Random number prefixing when writing
    - Allowing the hot keys to be distributed to different partitions
    - Read need to read from each partition and combine the result
    - Addition bookkeeping: Only apply the prefixing mechanism for hot keys

## Partitioning and Secondary Indexes

- Compare
    - Primary index → Search for a unique record
    - Secondary index → Search for occurrences of a particular value
- Many database avoid using secondary index for less complexity (HBase and Voldemort)

### Partitioning Secondary Indexes by Document (local index)

- Each partition maintains its own secondary indexes, covering only the documents in that partition
- Need to query all partitions and combine the results (scatter/gather)
    - Prone to tail latency amplification 👎
- Widely used: MongoDB, Riak, Cassandra, Elasticsearch, SolrCloud, and VoltDB

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%203.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%203.png)

Figure 6-4. Partitioning secondary indexes by document.

### Partitioning Secondary Indexes by Term (global index)

- A global index that covers data in all partitions
- Query term determines the partition of the index
- Partition the index by the term itself
- Read more efficiently 👍
- Need to write to multiple partitions 👎
- In practice, updates to global secondary indexes are often asynchronous

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%204.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%204.png)

Figure 6-5. Partitioning secondary indexes by term.

## Rebalancing Partitions

- Changes that lead to rebalancing
    - The Query throughput increases → CPU
    - The dataset size increase → Disks and RAM
    - Some machine fails
- Minimum Requirement

### Strategies for Rebalancing - Hash mod N

- Frequent moves when node number grows 👎

### Strategies for Rebalancing - Fixed number of partitions

- Create many more partitions than available nodes
- New node steal a few partitions from every existing node
- If a node is leaving, other node take the partition of that node (What if abnormal shutdown ?)
- Assignment is done asynchronously
- Choose the right partition to achieve best performance
    - Small partition for a small dataset. Larger partition are needed when the dataset grows

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%205.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%205.png)

Figure 6-6. Adding a new node to a database cluster with multiple partitions per node.

### Strategies for Rebalancing - Dynamic partitioning

- Split the partition when data of a partition reaches some threshold. Shrink otherwise.
- Process similar to B-Tree
- Empty database starts off with a single partition 👎
    - Solution: Initial set of partitions (pre-splitting)

### Strategies for Rebalancing - Partitioning proportionally to nodes

- Number of partitions is proportional to the number of nodes
- Partition shrinks when the node number increases
    - Randomly chooses a fixed number of existing partitions to split
- Hash-based partitioning is used

### Automatic or Manual Rebalancing

- Manual Rebalancing
    - Require administrator to configure
    - Only suggest partition assignment. EX: Couchbase, Riak, and Voldemort
- Fully automatically rebalancing
    - Less operational work 👍
    - High overload of rebalancing may harm performance 👎
        - EX: A slow responsive node detected as offline, and the rebalancing takes place
- Suggestion: Human should be involved

## Request Routing

- Approaches
    - Allow clients to contact any node. Each node then redirect the request to appropriate node
    - Send all requests from clients to a routing tier first
    - Clients be aware of the partitioning and the assignment of partitions
    to nodes
- Issue: How does the component making the routing decision learn about changes
in the assignment of partitions to nodes?

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%206.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%206.png)

Figure 6-7. Three different ways of routing a request to the right node.

- An separate service to coordinate: Zoo-keeper
    - Each node registers itself in ZooKeeper
    - ZooKeeper maintains the authoritative mapping of partitions to nodes
    - Routing tier or the partitioning-aware client, can subscribe to this information in ZooKeeper
    - DNS is usually used

![Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%207.png](Chp6%20-%20Partitioning%20327cb551b065456b8c0df2a9b1c10e57/Untitled%207.png)

Figure 6-8. Using ZooKeeper to keep track of assignment of partitions to nodes.

### Parallel Query Execution

- Massively parallel processing (MPP) relational database
- Query optimizer breaks this complex query into a number
of execution stages and partitions

## Summary

- Approaches to partitioning
    - Key range partitioning
    - Hash partitioning
    - Hybrid approaches
- Secondary index partitioning
    - Document-partitioned indexes
    - Term-partitioned indexes
- Rebalancing
    - Fixed number of partitions
    - Dynamic partitioning
- Routing
- TODO: What if write to multiple partitions fail