## Record
* Start on 2020/07/16

## Chapter
* Chap2 - Data Models and Query Languages: [Link](chp2/DDIA - chp 2 5c53dd04c0c64f329211db07c8dcc13f.md)
* Chap4 - Encoding and Evolution: [Link](chp4/Encoding and Evolution.pdf)
* Chap5 - Replication: [Link](chp5/Distributed Data.pdf)
* Chap6 - Partition: [Link](chp6/Chp6 - Partitioning 327cb551b065456b8c0df2a9b1c10e57.md)
